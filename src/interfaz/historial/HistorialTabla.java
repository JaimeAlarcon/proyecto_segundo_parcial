/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz.historial;

import java.io.IOException;
import java.util.ArrayList;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

 /**
 *
 * @author 
 * Fecha: 
 * Clase HistorialTabla presenta una nueva escena con el historial
 */
public class HistorialTabla 
{   
    private static Stage stage;
    private static Button aceptar;
    
    public static void generarTable() 
    {
        StackPane root = new StackPane();
        

        //root.setStyle("-fx-alignment: CENTER; -fx-background-color: black;");
        VBox central = new VBox();
        aceptar = new Button("ACEPTAR");
        
        stage = new Stage();
        usuario.Usuario.setListaUsuarios();
        ObservableList<usuario.Usuario> historial = FXCollections.observableArrayList(
            usuario.Usuario.getListaUsuarios());
        ListView<usuario.Usuario> tabla = new ListView<usuario.Usuario> (historial);
        central.getChildren().addAll(tabla, aceptar);
        root.getChildren().add(central);
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("HISTORIAL");
        stage.show();
        
        central.setPadding(new Insets(10, 10, 10, 10));
        root.setPadding(new Insets(10, 10, 10, 10));
        
        
    }
    
    public static Button getButtonAceptar()
    {
        return aceptar;
    }
    
    public static Stage getStage()
    {
        return stage;
    }
}
