/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;

/**
 *
 * @author elabandonao
 */

import interfaz.historial.HistorialTabla;
import interfaz.escenario.Nivel1;
import interfaz.menuPrincipal.MenuPrincipal;
import java.io.File;
import java.util.Optional;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class Show extends Application
{
    
    /**
 *
 * Creación de la escena No 1 del juego mediante la sobreescritura del método start e implementación del método Menú Principal
 * Fecha: 13-08-2018
 */
    @Override 
    public void start(Stage stage)
    {           
        MenuPrincipal.show();
        
        Scene scene = new Scene(MenuPrincipal.getRootMenuPrincipal(), 600, 600);
        
        //MenuPrincipal.getRootMenuPrincipal().setStyle("-fx-background-image: url('ciudad.jpg');");
        
        MenuPrincipal.getRootMenuPrincipal().setStyle("-fx-alignment: CENTER; -fx-background-color: gray;");
        stage.setScene(scene);
        stage.fullScreenExitHintProperty();
        stage.setResizable(false);
        stage.show();
        
        MenuPrincipal.getButtonJugar().setOnAction(e ->
            {
                interfaz.Usuario.show();
                //interfaz.escenario.Nivel1.show();
               
                
                
            }
        );        
        
        MenuPrincipal.getButtonHistorial().setOnMouseClicked(e -> 
            {
                HistorialTabla.generarTable();
                
                HistorialTabla.getButtonAceptar().setOnMouseClicked(d ->
                    HistorialTabla.getStage().close());
            });
        MenuPrincipal.getButtonSalir().setOnMouseClicked(e -> 
        {
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Confirmación");
            alert.setContentText("Desea salir del juego");
            stage.close();
            
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get()==ButtonType.OK)
            {
                stage.close();
            }
            else
            {
                stage.show();
            }
        });
    }
    
    public static void main(String[] args)
    {
        Application.launch(args);
    }
}