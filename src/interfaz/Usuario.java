package interfaz;

import interfaz.escenario.Nivel1;
import interfaz.escenario.Seleccion_Personaje;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
/**
 *
 * @author Edison Barreiro
 * Clase usuario, muestra la escena en donde se registra el jugador
 */
public class Usuario 
{    
    private static Stage stageUsuario;
    private static Scene sceneUsuario;
    private static String usuario;
    private static StackPane rootUsuario;
    private static TextField text;
    private static Button continuar;
    private static Button regresar;
       
    public static void show()
    {
        stageUsuario = new Stage();
        rootUsuario = new StackPane();
        rootUsuario.setStyle("-fx-alignment: CENTER; -fx-background-color: gray;");
        VBox root2 = new VBox();
        Label label = new Label("Ingrese usuario");
        label.setPadding(new Insets(0, 0, 0, 180));
        text = new TextField();
        root2.getChildren().addAll(label, text);
        HBox botones = new HBox();
        continuar = new Button("CONTINUAR");
        regresar = new Button("REGRESAR");
        botones.getChildren().addAll(regresar, continuar);
        
        botones.setPadding(new Insets(5, 5, 5, 5));
        
        root2.getChildren().addAll(botones);
        
        root2.setPadding(new Insets(5, 5, 5, 5));
        
        rootUsuario.getChildren().add(root2);
        
        sceneUsuario = new Scene(rootUsuario, 500, 100);
        
        stageUsuario.setScene(sceneUsuario);
        //stageUsuario.show();  
        
        rootUsuario.setPadding(new Insets(20, 20, 20, 20));
        //continuar();
        
        continuar.setOnAction(e -> 
        {
           
            ///stageUsuario.setScene(getSceneNivel1());
            System.out.println("El usuario ingresará con el nombre de: "+getTextUsuario());
            //stageUsuario.close();
            Seleccion_Personaje s= new Seleccion_Personaje();
            ///Scene sc= new Scene(s.getRoot(),500,500);
            stageUsuario.setScene(s.getPantalla());
            //stageUsuario.close();
            
        });
        //regresar();
         stageUsuario.show();
    }
    
    public static StackPane getRootUsuario()
    {
        return rootUsuario;
    }
    
    public static String getTextUsuario()
    {
        return text.getText();
    }
    
    public static void continuar()
    {
        
    }
    
    public static void regresar()
    {
        regresar.setOnMouseClicked(e -> stageUsuario.close());
    }

    public static Stage getStageUsuario() {
        return stageUsuario;
    }
    
}
