/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz.menuPrincipal;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;

/**
 *
 * @author elabandonao
 */
public class MenuPrincipal 
{
    
    private static Pane menuPrincipal;
    private static Button btnJugar;
    private static Button btnHistorial;
    private static Button btnSalir;
    
    public static void show()
    {
        menuPrincipal = new Pane();
        
        btnJugar = new Button("JUGAR");
        btnJugar.setLayoutX(260);
        btnJugar.setLayoutY(150);
        
        btnHistorial= new Button("HISTORIAL");
        btnHistorial.setLayoutX(260);
        btnHistorial.setLayoutY(250);
        
        btnSalir = new Button("SALIR");
        btnSalir.setLayoutX(260);
        btnSalir.setLayoutY(350);
        
        menuPrincipal.getChildren().addAll(btnJugar,
                btnHistorial, btnSalir);
        
    }    
        
    public static Pane getRootMenuPrincipal()
    {
        return menuPrincipal;
    }

    public static Button getButtonJugar()
    {
        return btnJugar;
    }
    
    public static Button getButtonHistorial()
    {
        return btnHistorial;
    }
    
    public static Button getButtonSalir()
    {
        return btnSalir;
    }
}
