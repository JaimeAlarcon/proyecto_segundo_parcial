/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz.escenario;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Jaime Alarcón
 */
public class ILevelUp {
    private ImageView imagenLevelUp;
    private static final String ARCHIVO_IMAGEN="/imagenes/level.png";

    public ILevelUp() {
        iniciar();
    }
    public void iniciar(){
        imagenLevelUp= new ImageView(new Image(ILevelUp.class.getResource(ARCHIVO_IMAGEN).toExternalForm()));
        imagenLevelUp.setFitHeight(500);
        imagenLevelUp.setFitWidth(500);
        
    }

    public ImageView getImagenLevelUp() {
        return imagenLevelUp;
    }

    public static String getARCHIVO_IMAGEN() {
        return ARCHIVO_IMAGEN;
    }
    
    
}
