/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz.escenario;

import Entidades.Recompensas.Escudo;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Jaime Alarcón
 */
public class ImGameOver {
    private ImageView imagenGameover;
    private ImageView fondo1;
    private ImageView fondo2;
    private ImageView fondo3;
    private ImageView fondo4;
    private static final String ARCHIVO_IMAGEN="/imagenes/Game.png";
    private static final String IMAGEN_FONDO="/imagenes/fondo.jpg";

    public ImGameOver() {
        inicializarElementos();
    }

    private void inicializarElementos() {
        imagenGameover= new ImageView(new Image(ImGameOver.class.getResource(ARCHIVO_IMAGEN).toExternalForm()));
        imagenGameover.setFitHeight(500);
        imagenGameover.setFitWidth(500);
        fondo1=new ImageView(new Image(ImGameOver.class.getResource(IMAGEN_FONDO).toExternalForm()));
        fondo1.setFitHeight(50);
        fondo1.setFitWidth(500);
        fondo2=new ImageView(new Image(ImGameOver.class.getResource(IMAGEN_FONDO).toExternalForm()));
        fondo2.setFitHeight(50);
        fondo2.setFitWidth(500);
        fondo3=new ImageView(new Image(ImGameOver.class.getResource(IMAGEN_FONDO).toExternalForm()));
        fondo3.setFitHeight(500);
        fondo3.setFitWidth(50);
        fondo4=new ImageView(new Image(ImGameOver.class.getResource(IMAGEN_FONDO).toExternalForm()));
        fondo4.setFitHeight(500);
        fondo4.setFitWidth(50);
        
    }

    public ImageView getImagenGameover() {
        return imagenGameover;
    }

    public static String getARCHIVO_IMAGEN() {
        return ARCHIVO_IMAGEN;
    }

    public ImageView getFondo1() {
        return fondo1;
    }

    public ImageView getFondo2() {
        return fondo2;
    }

    public ImageView getFondo3() {
        return fondo3;
    }

    public ImageView getFondo4() {
        return fondo4;
    }
    
  
 
}
