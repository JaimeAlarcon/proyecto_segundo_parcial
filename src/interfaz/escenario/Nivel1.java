package interfaz.escenario;

import Entidades.Animales.Gallina;
import Entidades.Animales.Gato;
import Entidades.Animales.Vaca;
import Entidades.Recompensas.Congelador;
import Entidades.Recompensas.Escudo;
import Entidades.Recompensas.Moneda;
import Entidades.Recompensas.Vida;
import Entidades.Vehiculos.Auto;
import Entidades.Vehiculos.Motocicleta;
import java.util.ArrayList;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import interfaz.escenario.Seleccion_Personaje;
import static interfaz.escenario.Seleccion_Personaje.tipo;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.event.EventType;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.Shape;
import java.util.Random;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import interfaz.escenario.Seleccion_Personaje;
import java.util.Timer;
import java.util.TimerTask;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.util.Duration;
import javax.swing.JOptionPane;
 /**
 *
 * @author Edison Barrerio
 * Fecha: 13-08-2018
 * Clase Nivel 1 que crea la escena del Nivel 1 del juego
 */
public class Nivel1 
{
    private  Pane rootNivel1; 
    private  Scene pantallanivel1;
    private  HBox  vidas;
    private HBox recompensas; 
    private Seleccion_Personaje s;
    private String tipo;
    private GameOver pgo;
    private LevelUp plu;
    private Gallina gallina;
    private Gato gato;
    private Vaca vaca;
    private int controlVidas;
    private int vidasxnivel;
    static Label lblClock;
    private  Date fechaDetenerse;
    protected static String nivel;
    private final SimpleDateFormat  dt = new SimpleDateFormat("HH:mm:ss"); ; 
    private ImageView imagen;
    private Rectangle shape;
    private AnimationTimer animationNivel1;
    private Rectangle grass;
    private Rectangle elementos;
    private Rectangle calle;
    private Line Carril5;
    private Rectangle callle2;
    private Circle obstaculo;
    private Circle obstaculo2;
    private Circle obstaculo3;
    
    private ArrayList<Circle> obstaculos;
    
    private Auto auto;
    private double autoPosX;
    private double autoPosY;
    private Rectangle shapeA;
    private double shapeAposX;
    private double shapeAposY;
        
    private Auto auto2;
    private double auto2PosX;
    private double auto2PosY;
    private Rectangle auto2Shape;
    private double auto2ShapePosX;
    private double auto2ShapePosY;
    
    private Motocicleta moto;
    private double motoPosX;
    private double motoPosY;
    private Rectangle motoShape;
    private double motoShapePosX;
    private double motoShapePosY;
   
    private Motocicleta moto2;
    private double moto2PosX;
    private double moto2PosY;
    private Rectangle moto2Shape;
    private double moto2ShapePosX;
    private double moto2ShapePosY;
   
    private double velocidad;
    
    private double carril1PosX;
    private double carrilPosY;
    private double carril2PosX;
    private double carril2PosY;
    
    private ArrayList<Moneda> monedas;
    private int sumaMonedas;
    
    private ArrayList<Escudo> escudos;
    private boolean protegido;
    
    private ArrayList<Congelador> congeladores;
    private boolean congelado;
    private int contadorCongelado;
    
    public Nivel1()
    {
        iniciaryorganizar();
    }
    public void iniciaryorganizar(){    
        rootNivel1 = new Pane();
        nivel= "1";
        
        Rectangle grass = new Rectangle(0, 0, 1200, 600);
        grass.setFill(Color.GREEN);
        
        Rectangle elementos = new Rectangle(0, 0, 1200, 100);
        elementos.setFill(Color.AQUA);
        
        Rectangle calle = new Rectangle(0, 200, 1200, 100);
        calle.setFill(Color.BLACK);
        
        ArrayList<Line> carriles = new ArrayList<>();
        int i = 0;
        while (i<=1200)
        {            
            Line carril = new Line(i, 250, i+55, 250);
           
            carril.setStroke(Color.WHITE);
            carril.setStrokeWidth(2);
            
            carriles.add(carril);
            i+= 110;
        }
        
        Rectangle calle2 = new Rectangle(0, 400, 1200, 100);
        
        ArrayList<Line> carriles2 = new ArrayList<>();
        i = 0;
        while (i<=1200)
        {            
            Line carril = new Line(i, 450, i+55, 450);
           
            carril.setStroke(Color.WHITE);
            carril.setStrokeWidth(2);
            
            carriles2.add(carril);
            i+= 110;
        }
        
        Circle obstaculo = new Circle(200, 350, 30);
        obstaculo.setFill(Color.WHITE);
        
        Circle obstaculo2 = new Circle(600, 350, 30);
        obstaculo2.setFill(Color.WHITE);
        
        Circle obstaculo3 = new Circle(1000, 555, 30);
        obstaculo3.setFill(Color.WHITE);
       
        rootNivel1.getChildren().addAll(grass, elementos, calle, 
                calle2, obstaculo, obstaculo2, obstaculo3);
        
        for (Line l:carriles)
        {
            rootNivel1.getChildren().add(l);
        }
        
        for (Line l:carriles2)
        {
            rootNivel1.getChildren().add(l);
        }
        
        tipo = Seleccion_Personaje.tipo;
        
        if(tipo.equals("Gallina"))
        {
            gallina = new Gallina();
            imagen = gallina.getImagengallina();
            shape = new Rectangle(510, 505, 65, 70);
        }
        else if(tipo.equals("Gato"))
        {
            gato = new Gato();
            imagen = gato.getImagengato();
            shape = new Rectangle(500, 502, 80, 80);
        }
        else if(tipo.equals("Vaca"))
        {
            vaca = new Vaca();
            imagen = vaca.getImagenvaca();
            shape = new Rectangle(500, 502, 80, 80);
        }
                
        imagen.setTranslateX(500);
        imagen.setTranslateY(500);
        
        rootNivel1.getChildren().add(imagen);
        
        pantallanivel1 = new Scene(rootNivel1, 1200, 600);
        
        carrilPosY = 200;
        carril2PosY = 400;
        
        auto = new Auto("auto", "BLACK", "auto");
        ImageView imagenCarro = auto.getImagencarro();
        autoPosX = 1200;
        autoPosY = carrilPosY;
        imagenCarro.setTranslateX(autoPosX);
        imagenCarro.setTranslateY(autoPosY);
        
        shapeAposX = 1200;
        shapeAposY = carrilPosY;
        shapeA = new Rectangle(shapeAposX, shapeAposY, 80, 80);
        shapeA.setFill(Color.CADETBLUE);
        
        auto2 = new Auto("auto 2", "GREEN", "auto");
        ImageView imagenCarro2 = auto2.getImagencarro();
        auto2PosX = 0;
        auto2PosY = carril2PosY;
        imagenCarro2.setTranslateX(auto2PosX);
        imagenCarro2.setTranslateY(auto2PosY);
        
        moto = new Motocicleta("motocicleta", "BLACK", "motocicleta");
        ImageView imagenMoto = moto.getImagenMoto();
        motoPosX = 2000;
        motoPosY = carril2PosY;
        imagenMoto.setTranslateX(500);
        imagenMoto.setTranslateY(carrilPosY);
        
        moto2 = new Motocicleta("motocicleta", "BLACK", "motocicleta");
        ImageView imagenMoto2 = moto2.getImagenMoto();
        moto2PosX = -800;
        moto2PosY = carril2PosY;
        imagenMoto2.setTranslateX(700);
        imagenMoto2.setTranslateY(carril2PosY);
        
        velocidad = 10;
        
        rootNivel1.getChildren().addAll(imagenCarro, imagenMoto, imagenCarro2, imagenMoto2,
                shapeA);
        
        obstaculos = new ArrayList<>();
        obstaculos.add(obstaculo);
        obstaculos.add(obstaculo2);
        obstaculos.add(obstaculo3);
        lblClock = new Label("00:00:00");
        System.out.println("Entró");
        try {
            Date now = dt.parse(lblClock.getText());
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(now);
            calendar.add(Calendar.SECOND,60);
            fechaDetenerse = calendar.getTime();
        } catch (ParseException ex) {
            Logger.getLogger(Nivel1.class.getName()).log(Level.SEVERE, null, ex);
        }
        //setFechaDetenerse();
        rootNivel1.getChildren().add(lblClock);
        lblClock.setLayoutX(1000);
        
        //lblClock.setLayoutY(1000);
        VidasIniciales();
        
        monedas = new ArrayList<>();
        escudos = new ArrayList<>();
        congeladores = new ArrayList<>();
        
        sumaMonedas = 0;
        contadorCongelado = 0;
        
            
        animationNivel1 = new AnimationTimer()
        {
            @Override
            public void handle(long now) 
            {
                if (congelar() == true)
                {
                    System.out.println("congelado");
                    animationNivel1.stop();
                    
                    Timer timer = new Timer();
                    timer.schedule(new TimerTask() {

                    @Override
                    public void run() {
                        Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            animationNivel1.start();
                    }
                        });

                    }
                    }, 3000);
                }
                
                if (proteger() == true)
                {
                    System.out.println("protegido");
                    protegido = true;
                    Timer timer = new Timer();
                    timer.schedule(new TimerTask() {

                    @Override
                    public void run() {
                        Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            protegido = false;
                            if ( (choque(imagen, imagenCarro)) == true ||
                                (choque(imagen, imagenCarro2)) == true ||
                                (choque(imagen, imagenMoto)) == true ||
                                (choque(imagen, imagenMoto2)) == true && protegido == true)
                                {   
                                    int contadorcol=0;
                                    //prueba(grafica) de conexion entre choques y vidas 
                                    System.out.println("una vida menos");
                                    String mensaje = null;
                                    controlVidas=0;
                                    System.out.println("Tipo: "+tipo);
                                    if(tipo.equals("Gallina")){
                                        eliminarvidaanimal(gallina,contadorcol);
                                    }
                    
                                    else if(tipo.equals("Gato")){
                                        eliminarvidaanimal(gato,contadorcol);
                       
                                    }
                                    else if(tipo.equals("Vaca")){
                                        eliminarvidaanimal(vaca,contadorcol);
                                    }
                                    
                                System.out.println(mensaje);
                                if(vidasxnivel==controlVidas){
                                JOptionPane.showMessageDialog(null,"Has perdido :( !!!!! "); 
                                //Platform.exit();
                                ///Stage go = new Stage();
                                //interfaz.Usuario.getStageUsuario().close();
                                animationNivel1.stop();
                                pgo= new GameOver();
                                //go.setScene(pgo.getPantallaGO());
                                //go.show();
                                interfaz.Usuario.getStageUsuario().setScene(pgo.getPantallaGO());
                                }}}});}}, 3000);}
                
                imagenCarro.setTranslateX(autoPosX);
                autoPosX -= velocidad;
                if (autoPosX <- 40)
                {
                    autoPosX = 1220;
                }                
                
                imagenCarro2.setTranslateX(auto2PosX);
                auto2PosX += velocidad;
                if (auto2PosX >= 1200)
                {
                    auto2PosX = 0;
                }
                
                imagenMoto.setTranslateX(motoPosX);
                motoPosX -= velocidad;
                if (motoPosX <= -40)
                {
                    motoPosX = 1220;
                }
                
                imagenMoto2.setTranslateX(moto2PosX);
                moto2PosX += velocidad;
                if (moto2PosX >= 1200)
                {
                    moto2PosX = 0;
                }
                      
                pantallanivel1.setOnKeyPressed( (KeyEvent event) -> 
                {
                    moverAnimal(event);
                });
                
                Shape colisionObstaculo1 = Shape.intersect(shape, obstaculo);
                boolean ocurrioColisionObstaculo1 = colisionObstaculo1.getBoundsInLocal().isEmpty();
                
                if (ocurrioColisionObstaculo1 == false)
                {
                    System.out.println("Choque con obstaculo 1");
                    System.out.println(shape.getTranslateX());
                    System.out.println(shape.getTranslateY());
                }
                
                Shape colisionObstaculo2 = Shape.intersect(shape, obstaculo2);
                boolean ocurrioColisionObstaculo2 = colisionObstaculo2.getBoundsInLocal().isEmpty();
                
                if (ocurrioColisionObstaculo2 == false)
                {
                    System.out.println("Choque con obstaculo 2");
                    System.out.println(shape.getTranslateX());
                    System.out.println(shape.getTranslateY());
                }
                
                Shape colisionObstaculo3 = Shape.intersect(shape, obstaculo3);
                boolean ocurrioColisionObstaculo3 = colisionObstaculo3.getBoundsInLocal().isEmpty();
                
                if (ocurrioColisionObstaculo3 == false)
                {
                    System.out.println("Choque con obstaculo 3");
                    System.out.println(shape.getTranslateX());
                    System.out.println(shape.getTranslateY());
                }
                
                Shape colisionConAuto = Shape.intersect(shape, shapeA);
                boolean ocurrioColisionConAuto = colisionConAuto.getBoundsInLocal().isEmpty();
                if (ocurrioColisionConAuto == false)
                {
                    System.out.println("Choque con auto 1");
                    System.out.println(shape.getTranslateX());
                    System.out.println(shape.getTranslateY());
                }
                
                sumarMonedas();
                System.out.println("******************");
                System.out.println("monedas: " + sumaMonedas);
            }
        };
        
        animationNivel1.start();
        
        Thread t = new Thread(new Executor());
        t.start();
    }
    
    //Metodo hecho por Edison Barreiro
    public void moverAnimal(KeyEvent event)
    {
        for (Circle c : obstaculos)
        {
            System.out.println(c.getBoundsInParent());
            if (imagen.getBoundsInParent().intersects(c.getBoundsInParent()) == true)
            {
                imagen.setTranslateX(500);
                imagen.setTranslateY(500);
            }
        }
        if (imagen.getTranslateX() < 10)
        {
            imagen.setTranslateX(10);
        }
        else if (imagen.getTranslateX() > (1110))
        {
            imagen.setTranslateX(1110);
        }
        else if (imagen.getTranslateY() > 500)
        {
            imagen.setTranslateY(500);
        }
        else if (imagen.getTranslateY() < 50)
        {
            System.out.println("Gano");
            JOptionPane.showMessageDialog(null,"GANASTE!!!!! ");
            animationNivel1.stop();
            plu= new LevelUp();
            interfaz.Usuario.getStageUsuario().setScene(plu.getPantallaLU());
            animationNivel1.stop();
           
        }
        else
        {
            switch(event.getCode())
            {
                case UP:
                    imagen.setTranslateY(imagen.getTranslateY() - 10);
                    shape.setTranslateY(shape.getTranslateY() - 10);
                    break;
                case RIGHT:
                    imagen.setTranslateX(imagen.getTranslateX() + 10);
                    shape.setTranslateX(shape.getTranslateX() + 10);
                    break;
                case DOWN:
                    imagen.setTranslateY(imagen.getTranslateY() + 10);
                    shape.setTranslateY(shape.getTranslateY() + 10);
                    break;
                case LEFT:
                    imagen.setTranslateX(imagen.getTranslateX() - 10);
                    shape.setTranslateX(shape.getTranslateX() - 10);
                    break;
            }  
        }
    }
    
    //Metodo hecho por Jaime Alarcon
    public void moverObjeto(KeyEvent e){
          if (e.getCode().equals(KeyCode.RIGHT)){
            imagen.setLayoutX(imagen.getLayoutX()+5);
            System.out.println("Moviendo");
        }
        else if (e.getCode().equals(KeyCode.LEFT)){
            imagen.setLayoutX(imagen.getLayoutX()-5);
            System.out.println("Moviendo"
                    + "");
            
        }
        else if (e.getCode().equals(KeyCode.UP)){
            imagen.setLayoutY(imagen.getLayoutY()-5);
            System.out.println("Moviendo"
                    + "");
            
        }
         else if (e.getCode().equals(KeyCode.DOWN)){
            imagen.setLayoutY(imagen.getLayoutY()+5);
            System.out.println("Moviendo"
                    + "");
            
        }   
        
    }
    
    public boolean choque()
    {
        return false;
    }
    
    public boolean choque(ImageView imagen1, ImageView imagen2)
    {
        if (imagen.getBoundsInParent().intersects(imagen2.getBoundsInParent()))
                {
                    System.out.println("Choque");
                    System.out.println("AnimalitoBoundsParent :" + imagen.getBoundsInParent());
                    System.out.println("ObstaculoBoundsParent :" + imagen2.getBoundsInParent());
                    imagen.setTranslateX(500);
                    imagen.setTranslateY(500);
                    return true;
                }
        return false;
    }
    
    public Scene getSceneNivel1()
    {
        return pantallanivel1;
    }

    public  Pane getRootNivel1() {
        return rootNivel1;
    }
    
    public void CrearEscudos(){
        Escudo e= new Escudo();
        ImageView imagen= e.getImagenescudo();
        rootNivel1.getChildren().add(e.getImagenescudo());
        imagen.setLayoutX(150+GenerarPosX());
        imagen.setLayoutY(150+GenerarPosY());
        escudos.add(e);
    }
    
    public void CrearMonedas(){
        Moneda m = new Moneda();
        ImageView imagen= m.getImagenmoneda();
        rootNivel1.getChildren().add(m.getImagenmoneda());
        imagen.setLayoutX(150+GenerarPosX());
        imagen.setLayoutY(150+GenerarPosY());
        monedas.add(m);
    }
    
    public void CrearCongelador(){
        Congelador c = new Congelador();
        ImageView imagen= c.getImagencongelador();
        rootNivel1.getChildren().add(c.getImagencongelador());
        imagen.setLayoutX(150+GenerarPosX());
        imagen.setLayoutY(150+GenerarPosY());
        congeladores.add(c);
    }
    
    public void CrearVida(){
        Vida vid= new Vida();
        ImageView imagen= vid.getImagenvida();
        //int y= r.nextInt(520);
        rootNivel1.getChildren().add(imagen);
        imagen.setLayoutX(150+GenerarPosX());
        imagen.setLayoutY(150+GenerarPosY());
        //imagen.setLayoutY(y);
        
        //rootNivel1.getChildren().add(vid.getImagenvida());
    }
    
    public void VidasIniciales(){
        vidas= new HBox();
        for(int i =0 ; i<3;i++){
            Vida v = new Vida();
            vidas.getChildren().add(v.getImagenvida());
        }
        rootNivel1.getChildren().add(vidas);
        
    }
    
    public void VidasIniciales(int vidasres){
        vidas= new HBox();
        for(int i=0;i<=vidasres;i++){
            Vida v = new Vida();
            vidas.getChildren().add(v.getImagenvida());
            
        }
        rootNivel1.getChildren().add(vidas);
    }
    
    public void anadirMoneda()
    {
        sumaMonedas += 1;
    }
    
    public void sumarMonedas()
    {
        
        for (Moneda m : monedas)
        {
            if (m.getImagenmoneda().getBoundsInParent().intersects(imagen.getBoundsInParent()) && m.getInterseccion() == false)
            {
                System.out.println("choque de imagen de animal con moneda");
                m.setInterseccion();
                anadirMoneda();
            }
            
            
        }
    }
    
    public boolean congelar()
    {
        boolean congelado = false;
        for (Congelador c : congeladores)
        {
            if (c.getImagencongelador().getBoundsInParent().intersects(imagen.getBoundsInParent()) && c.getInterseccion() == false)
            {
                c.setInterseccion();
                System.out.println("!!!!!congelado");
                congelado = true;
            }  
        }
        
        return congelado;
    }
    
    public boolean proteger()
    {
        boolean protegido = false;
        for (Escudo e : escudos)
        {
            if (e.getImagenescudo().getBoundsInParent().intersects(imagen.getBoundsInParent()) && e.getInterseccion() == false)
            {
                   protegido = true;
            }
        }
        
        return protegido;
    }
    
    public void eliminarvidaanimal(Gallina gallina,int contadorcol){
        String mensaje= null;
        if (contadorcol==0){
            vidas.getChildren().remove(0);
            gallina.setVidas(gallina.getVidas()-1);
            System.out.println("Vidas; "+ gallina.getVidas());
            mensaje = tipo +" una vida menos";
            vidasxnivel=gallina.getVidas();
            contadorcol++;
                            
                        }
        else if (contadorcol==1){
            vidas.getChildren().remove(1);
            gallina.setVidas(gallina.getVidas()-1);
            System.out.println("Vidas; "+ gallina.getVidas());
            mensaje=tipo +" una vida menos";
            vidasxnivel=gallina.getVidas();
            contadorcol++;
                        }
        else if (contadorcol==2){
            vidas.getChildren().remove(0);
            gallina.setVidas(gallina.getVidas()-1);
            System.out.println("Vidas; "+ gallina.getVidas());
            mensaje=tipo +" una vida menos";
            vidasxnivel=gallina.getVidas();
            contadorcol++;
                        
                        }
        
    }
    
    public void eliminarvidaanimal(Gato gato,int contadorcol){
        String mensaje= null;
        if (contadorcol==0){
            vidas.getChildren().remove(0);
            gato.setVidas(gato.getVidas()-1);
            System.out.println("Vidas; "+ gato.getVidas());
            mensaje = tipo +" una vida menos";
            vidasxnivel=gato.getVidas();
            contadorcol++;
                            
                        }
        else if (contadorcol==1){
            vidas.getChildren().remove(1);
            gato.setVidas(gato.getVidas()-1);
            System.out.println("Vidas; "+ gato.getVidas());
            mensaje=tipo +" una vida menos";
            vidasxnivel=gato.getVidas();
            contadorcol++;
                        }
        else if (contadorcol==2){
            vidas.getChildren().remove(0);
            gato.setVidas(gato.getVidas()-1);
            System.out.println("Vidas; "+ gato.getVidas());
            mensaje=tipo +" una vida menos";
            vidasxnivel=gato.getVidas();
            contadorcol++;
                        
                        }
    }
    
    public void eliminarvidaanimal(Vaca vaca,int contadorcol){
        String mensaje= null;
        if (contadorcol==0){
            vidas.getChildren().remove(0);
            vaca.setVidas(vaca.getVidas()-1);
            System.out.println("Vidas; "+ vaca.getVidas());
            mensaje = tipo +" una vida menos";
            vidasxnivel=vaca.getVidas();
            contadorcol++;
                            
                        }
        else if (contadorcol==1){
            vidas.getChildren().remove(1);
            vaca.setVidas(vaca.getVidas()-1);
            System.out.println("Vidas; "+ vaca.getVidas());
            mensaje=tipo +" una vida menos";
            vidasxnivel=vaca.getVidas();
            contadorcol++;
                        }
        else if (contadorcol==2){
            vidas.getChildren().remove(0);
            vaca.setVidas(vaca.getVidas()-1);
            System.out.println("Vidas; "+ vaca.getVidas());
            mensaje=tipo +" una vida menos";
            vidasxnivel=vaca.getVidas();
            contadorcol++;
                        
                        }
    }
    
    void setFechaDetenerse(){
        System.out.println("Entró");
        try {
            Date now = dt.parse(lblClock.getText());
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(now);
            calendar.add(Calendar.SECOND,60);
            fechaDetenerse = calendar.getTime();
        } catch (ParseException ex) {
            Logger.getLogger(Nivel1.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public int GenerarPosX(){
        int x=0;
        Random r = new Random();
        x= r.nextInt(970);
        return x;
    }
    
    public int GenerarPosY(){
        int y=0;
        Random r1 = new Random();
        y= r1.nextInt(370);
        return y;
    }

    public static Label getLblClock() {
        return lblClock;
    }

    public static void setLblClock(Label lblClock) {
        Nivel1.lblClock = lblClock;
    }
    
    
    
    class Executor implements Runnable{

        @Override
        public void run() {
            JOptionPane.showMessageDialog(null, "Cargando Componentes espere un momento!");
          
             ///Platform.runLater(()->setFechaDetenerse());
            for(int i=0; i<2;i++){
                try{
                    Platform.runLater(()->CrearVida());
                    Platform.runLater(()->CrearMonedas());
                    //Platform.runLater(()->CrearEscudos());
                    //Platform.runLater(()->CrearCongelador());
                    System.out.println("Vida");
                    //System.out.println("Monedas");
                    //System.out.println("Escudos");
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Nivel1.class.getName()).log(Level.SEVERE, null, ex);
                }
            
        }
            
            for(int k=0;k<8;k++){
                try{
                    Platform.runLater(()->CrearMonedas());
                    System.out.println("Monedas");
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Nivel1.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
            
            for(int j=0; j<2; j++){
                try{
                    Platform.runLater(()->CrearEscudos());
                    System.out.println("Escudo");
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Nivel1.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            for(int a=0;a<2;a++){
                try{
                    Platform.runLater(()->CrearCongelador());
                    System.out.println("Congelador");
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Nivel1.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
           JOptionPane.showMessageDialog(null,"Componentes cargados a Jugar");
        
        }
        
    }
    
}
