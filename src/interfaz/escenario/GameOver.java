/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz.escenario;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;

/**
 *
 * @author Jaime Alarcón
 */
public class GameOver {
    private BorderPane rootgo;
    private  StackPane panelimagen;
    private  StackPane panelimagena;
    private  StackPane panelimagenab;
    private  StackPane panelimageni;
    private  StackPane panelimagend;
    private HBox contentb;
    private ImGameOver imgo;
    private Button reintentar;
    private Scene pantallaGO;
    private Button salir;
    private Label mensaje;

    public GameOver() {
        generar();
    }
    public void generar(){
        rootgo= new BorderPane();
        panelimagen= new StackPane();
        panelimagena= new StackPane();
        panelimagenab= new StackPane();
        panelimageni= new StackPane();
        panelimagend= new StackPane();
        imgo= new ImGameOver();
        contentb= new HBox();
        contentb.setAlignment(Pos.CENTER);
        reintentar= new Button("Reintentar");
        salir= new Button("Salir");
        contentb.getChildren().addAll(reintentar,salir);
        mensaje= new Label("¿Qué desea hacer?");
        mensaje.setTextFill(Color.WHITESMOKE);
        panelimagen.getChildren().add(imgo.getImagenGameover());
        panelimagena.getChildren().add(imgo.getFondo1());
        panelimagena.getChildren().add(mensaje);
        panelimagenab.getChildren().add(imgo.getFondo2());
        panelimagenab.getChildren().add(contentb);
        panelimageni.getChildren().add(imgo.getFondo3());
        panelimagend.getChildren().add(imgo.getFondo4());
        
        rootgo.setTop(panelimagena);
        rootgo.setBottom(panelimagenab);
        rootgo.setCenter(panelimagen);
        rootgo.setRight(panelimagend);
        rootgo.setLeft(panelimageni);
        //rootgo.setBottom(panelimagenab);
        ///panel.getChildren().addAll(reintentar,salir,mensaje);
        
        reintentar.setOnAction(e->{
            Nivel1 n1= new Nivel1();
            interfaz.Usuario.getStageUsuario().setScene(n1.getSceneNivel1());
        });
        salir.setOnAction(e->{
            interfaz.Usuario.getStageUsuario().close();
        });
        
        
        pantallaGO = new Scene(rootgo,600,600);
        
    }

    public ImGameOver getImgo() {
        return imgo;
    }

    public Scene getPantallaGO() {
        return pantallaGO;
    }
    
    
    
    
    
}
