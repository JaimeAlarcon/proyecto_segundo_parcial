/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz.escenario;

import Entidades.Animales.Gallina;
import Entidades.Animales.Gato;
import Entidades.Animales.Vaca;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import static javafx.print.PrintColor.COLOR;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.Rectangle;
import javax.swing.JOptionPane;

/**
 *
 * @author Jaime Alarcón
 */
public class Seleccion_Personaje {
    private BorderPane root;
    private GridPane contain;
    protected static String tipo;
    private Gallina gallina;
    private Gato gato;
    private Vaca vaca;
    private ToggleGroup animales;
    private RadioButton rbGallina;
    private RadioButton rbGato;
    private RadioButton rbVaca;
    private Button Aceptar;
    private Scene pantalla;
    public Seleccion_Personaje(){
       iniciaryOrganizar(); 
    }
    public void iniciaryOrganizar(){
        root= new BorderPane();
        contain= new GridPane();
        Label titulo= new Label("Selecciona tu personaje");
        gallina= new Gallina();
        gato= new Gato();
        vaca= new Vaca();
        Rectangle r1= new Rectangle();
        Rectangle r2= new Rectangle();
        Rectangle r3= new Rectangle();
        r1.setStyle("-fx-background-color: GREEN;");
        r2.setStyle("-fx-background-color: BLUE;");
        r3.setStyle("-fx-background-color: RED;");
        animales = new ToggleGroup() ; 
        Aceptar= new Button("Aceptar");
        rbGallina= new RadioButton("Gallina");
        rbGallina.setSelected(true);
        rbGato= new RadioButton("Gato");
        rbVaca= new RadioButton("Vaca");
        rbGallina.setToggleGroup(animales);
        rbGato.setToggleGroup(animales);
        rbVaca.setToggleGroup(animales);
        contain.setMinSize(500, 500) ; 
        root.setBottom(Aceptar);
      //Espaciado Horizontal 
      
        contain.setPadding(new Insets(10, 10, 10, 10) );  
        contain.setVgap(5) ; 
        contain.setHgap(5) ;       
        contain.setAlignment(Pos.CENTER) ; 
        contain.add(gallina.getImagengallina(), 3, 1);
        contain.add(gato.getImagengato(), 3, 2);
        contain.add(vaca.getImagenvaca(), 3, 3);
        
        contain.add(rbGallina, 4, 1);
        contain.add(rbGato, 4, 2);
        contain.add(rbVaca,4,3);
        
        
        root.setCenter(contain);
        root.setTop(titulo);
  
        
        pantalla= new Scene(root,500,500);
        
        Aceptar.setOnAction(e->{
            System.out.println("abriendo nivel");
                  if (rbGallina.isSelected()) {
            tipo="Gallina";
        } else if (rbGato.isSelected()) {
            tipo="Gato";
        } else if (rbVaca.isSelected()) {
            tipo="Vaca";
        }
        
        JOptionPane.showMessageDialog(null,"Personaje escogido: "+tipo);
            Nivel1 n1 = new Nivel1();
            interfaz.Usuario.getStageUsuario().setScene(n1.getSceneNivel1());
        });
        
    }

    public BorderPane getRoot() {
        return root;
    }

    public void setRoot(BorderPane root) {
        this.root = root;
    }

    public Scene getPantalla() {
        return pantalla;
    }

    public void setPantalla(Scene pantalla) {
        this.pantalla = pantalla;
    }
    
    
    
}
//}
