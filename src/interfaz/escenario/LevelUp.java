/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz.escenario;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import interfaz.escenario.Nivel1;
import interfaz.escenario.Nivel2;

/**
 *
 * @author Jaime Alarcón
 */
public class LevelUp {
    private BorderPane rootlu;
    private  StackPane panelimagen;
    private StackPane Botones;
    private HBox contentb;
    private ILevelUp lu;
    private Button siguiente;
    private Scene pantallaLU;
    private Button salir;
    private Label mensaje;
    public LevelUp(){
        iniciar();
    }
    public void iniciar(){
        rootlu= new BorderPane();
        panelimagen= new StackPane();
        Botones= new StackPane();
        contentb= new HBox();
        lu = new ILevelUp();
        siguiente = new Button("Siguiente Nivel");
        salir= new Button("Salir");
        mensaje= new Label("Estás listo para más?");
        mensaje.setAlignment(Pos.CENTER);
        panelimagen.getChildren().add(lu.getImagenLevelUp());
        contentb.getChildren().addAll(salir,siguiente);
        contentb.setAlignment(Pos.CENTER);
        Botones.getChildren().add(contentb);
        rootlu.setTop(mensaje);
        rootlu.setCenter(panelimagen);
        rootlu.setBottom(Botones);
        siguiente.setOnAction(e->{
            System.out.println("Pendiente Analizar Cambio de Nivel");
            if(Nivel1.nivel.equals("1")){
                Nivel2 n2= new Nivel2();
                interfaz.Usuario.getStageUsuario().setTitle("Nivel2");
                interfaz.Usuario.getStageUsuario().setScene(n2.getSceneNivel2());
                
            }
            else if(Nivel2.nivel.equals("2")){
                Nivel3 n3= new Nivel3();
                interfaz.Usuario.getStageUsuario().setTitle("Nivel3");
                interfaz.Usuario.getStageUsuario().setScene(n3.getSceneNivel3());
                
            }
            //Nivel1 n1= new Nivel1();
            //interfaz.Usuario.getStageUsuario().setScene(n1.getSceneNivel1());
        });
        salir.setOnAction(e->{
            interfaz.Usuario.getStageUsuario().close();
        });
        pantallaLU = new Scene(rootlu,600,600);
        
        
        
        
        
    }

    public BorderPane getRootlu() {
        return rootlu;
    }

    public StackPane getPanelimagen() {
        return panelimagen;
    }

    public StackPane getBotones() {
        return Botones;
    }

    public HBox getContentb() {
        return contentb;
    }

    public ILevelUp getLu() {
        return lu;
    }

    public Button getSiguiente() {
        return siguiente;
    }

    public Scene getPantallaLU() {
        return pantallaLU;
    }

    public Button getSalir() {
        return salir;
    }

    public Label getMensaje() {
        return mensaje;
    }
    

    
}
