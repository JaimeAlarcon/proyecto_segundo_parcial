/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz.escenario;

import Entidades.Animales.Gallina;
import Entidades.Animales.Gato;
import Entidades.Animales.Vaca;
import Entidades.Vehiculos.Auto;
import Entidades.Vehiculos.Motocicleta;
import interfaz.escenario.Seleccion_Personaje;
import java.util.ArrayList;
import java.util.Random;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.stage.Stage;

/**
 *
 * @author elabandonao
 */
public class Nivel2_Prueba1 extends Application {
    
    private  Pane rootNivel1; 
    private  Scene pantallanivel1;
    private Seleccion_Personaje s;
    private String tipo;
    
    private Gallina gallina;
    private Gato gato;
    private Vaca vaca;
    private Rectangle shape;
    private ImageView imagen;
    private double imagenPosX;
    private double imagenPosY;
    
    private Rectangle grass;
    private Rectangle elementos;
    private Rectangle calle;
    private Line Carril5;
    private Rectangle callle2;
    private Circle obstaculo;
    private Circle obstaculo2;
    private Circle obstaculo3;
    private Circle obstaculo4;
    private Circle obstaculo5;
    private Circle obstaculo6;
    
    private ArrayList<Shape> obstaculos;
    
    private Auto auto;
    private double autoPosX;
    private double autoPosY;
    
    
    private Auto auto2;
    private double auto2PosX;
    private double auto2PosY;
    private Rectangle auto2Shape;
    private double auto2ShapePosX;
    private double auto2ShapePosY;
    
    private Motocicleta moto;
    private double motoPosX;
    private double motoPosY;
    private Rectangle motoShape;
    private double motoShapePosX;
    private double motoShapePosY;
   
    private Motocicleta moto2;
    private double moto2PosX;
    private double moto2PosY;
    private Rectangle moto2Shape;
    private double moto2ShapePosX;
    private double moto2ShapePosY;
    
    private double velocidadCarrilInferior;
    private double velocidadCarrilSuperior;
    
    private double carril1PosX;
    private double carrilPosY;
    private double carril2PosX;
    private double carril2PosY;
    
    private boolean choque;
    
    private AnimationTimer animationNivel1;
    
    
    @Override
    public void start(Stage stage) 
    {
        rootNivel1 = new Pane();
        Rectangle grass = new Rectangle(0, 0, 1200, 600);
        grass.setFill(Color.GREEN);
        
        Rectangle elementos = new Rectangle(0, 0, 1200, 100);
        elementos.setFill(Color.AQUA);
        
        Rectangle calle = new Rectangle(0, 200, 1200, 100);
        calle.setFill(Color.BLACK);
        
        ArrayList<Line> carriles = new ArrayList<>();
        int i = 0;
        while (i<=1200)
        {            
            Line carril = new Line(i, 250, i+55, 250);
           
            carril.setStroke(Color.WHITE);
            carril.setStrokeWidth(2);
            
            carriles.add(carril);
            i+= 110;
        }
        
        Rectangle calle2 = new Rectangle(0, 400, 1200, 100);
        
        ArrayList<Line> carriles2 = new ArrayList<>();
        i = 0;
        while (i<=1200)
        {            
            Line carril = new Line(i, 450, i+55, 450);
           
            carril.setStroke(Color.WHITE);
            carril.setStrokeWidth(2);
            
            carriles2.add(carril);
            i+= 110;
        }
        
        
        ArrayList<Integer> posicionParking = new ArrayList<>();
        posicionParking.add(200);
        posicionParking.add(600);
        Random rn = new Random();
        int al = rn.nextInt(2);
        Rectangle parking = new Rectangle(posicionParking.get(al), 100, 400, 90);
        parking.setFill(Color.GRAY);
        
        Line linea = new Line((posicionParking.get(al) + 100), 100,
            (posicionParking.get(al) + 100), 190);
        linea.setFill(Color.YELLOW);
        Line linea2 = new Line((posicionParking.get(al) + 200), 100,
            (posicionParking.get(al) + 200), 190);
        linea.setFill(Color.YELLOW);
        Line linea3 = new Line((posicionParking.get(al) + 300), 100,
            (posicionParking.get(al) + 300), 190);
        linea.setFill(Color.YELLOW);
        Line linea4 = new Line((posicionParking.get(al) + 400), 100,
            (posicionParking.get(al) + 400), 190);
        linea.setFill(Color.YELLOW);
        
        Rectangle amarillo = new Rectangle(posicionParking.get(al), 190, 50, 10);
        amarillo.setFill(Color.YELLOW);
        Rectangle rojo = new Rectangle((posicionParking.get(al) + 50), 190, 50, 10);
        rojo.setFill(Color.RED);
        Rectangle amarillo2 = new Rectangle((posicionParking.get(al) + 100), 190, 50, 10);
        amarillo2.setFill(Color.YELLOW);
        Rectangle rojo2 = new Rectangle((posicionParking.get(al) + 150), 190, 50, 10);
        rojo2.setFill(Color.RED);
        Rectangle amarillo3 = new Rectangle((posicionParking.get(al) + 200), 190, 50, 10);
        amarillo3.setFill(Color.YELLOW);
        Rectangle rojo3 = new Rectangle((posicionParking.get(al) + 250), 190, 50, 10);
        rojo3.setFill(Color.RED);
        Rectangle amarillo4 = new Rectangle((posicionParking.get(al) + 300), 190, 50, 10);
        amarillo4.setFill(Color.YELLOW);
        Rectangle rojo4 = new Rectangle((posicionParking.get(al) + 350), 190, 50, 10);
        rojo4.setFill(Color.RED);
        
        Circle obstaculo = new Circle(200, 350, 30);
        obstaculo.setFill(Color.WHITE);
        
        Circle obstaculo2 = new Circle(800, 350, 30);
        obstaculo2.setFill(Color.WHITE);
        
        Circle obstaculo3 = new Circle(1100, 555, 30);
        obstaculo3.setFill(Color.WHITE);
        
        Circle obstaculo4 = new Circle(400, 350, 30);
        obstaculo4.setFill(Color.WHITE);
        
        Circle obstaculo5 = new Circle(1000, 350, 30);
        obstaculo5.setFill(Color.WHITE);
        
        Circle obstaculo6 = new Circle(1100, 150, 30);
        obstaculo6.setFill(Color.WHITE);
        
        Circle obstaculo7 = new Circle(100, 150, 30);
        obstaculo7.setFill(Color.WHITE);
        
        Circle obstaculo8 = new Circle(100, 550, 30);
        obstaculo8.setFill(Color.WHITE);
        
        rootNivel1.getChildren().addAll(grass, elementos, parking, amarillo,
                rojo, amarillo2, rojo2, amarillo3, rojo3, amarillo4,
                rojo4, linea, linea2, linea3, linea4, calle,
                calle2, obstaculo, obstaculo2, obstaculo3, obstaculo4,
                obstaculo5, obstaculo6, obstaculo7, obstaculo8);
        
        for (Line l:carriles)
        {
            rootNivel1.getChildren().add(l);
        }
        
        for (Line l:carriles2)
        {
            rootNivel1.getChildren().add(l);
        }
        
        //Gallina gallina = new Gallina();
        //Vaca vaca = new Vaca();
        Gato gato = new Gato();
        imagen = gato.getImagengato();
        imagen.setTranslateX(500);
        imagen.setTranslateY(500);
        
        shape = new Rectangle(500, 502, 80, 80);
        shape.setFill(Color.CADETBLUE);
        shape.setVisible(false);
        rootNivel1.getChildren().add(shape);
        
        rootNivel1.getChildren().add(imagen);
        
        pantallanivel1 = new Scene(rootNivel1, 1200, 600);
        
        carrilPosY = 200;
        carril2PosY = 400;
        
        auto = new Auto("auto", "BLACK", "auto");
        ImageView imagenCarro = auto.getImagencarro();
        autoPosX = 1200;
        autoPosY = carrilPosY;
        imagenCarro.setTranslateX(autoPosX);
        imagenCarro.setTranslateY(autoPosY);
        
        auto2 = new Auto("auto 2", "GREEN", "auto");
        ImageView imagenCarro2 = auto2.getImagencarro();
        auto2PosX = 0;
        auto2PosY = carril2PosY;
        imagenCarro2.setTranslateX(auto2PosX);
        imagenCarro2.setTranslateY(auto2PosY);
        
        moto = new Motocicleta("motocicleta", "BLACK", "motocicleta");
        ImageView imagenMoto = moto.getImagenMoto();
        motoPosX = 2000;
        motoPosY = carril2PosY;
        imagenMoto.setTranslateX(500);
        imagenMoto.setTranslateY(carrilPosY);
        
        moto2 = new Motocicleta("motocicleta", "BLACK", "motocicleta");
        ImageView imagenMoto2 = moto2.getImagenMoto();
        moto2PosX = -800;
        moto2PosY = carril2PosY;
        imagenMoto2.setTranslateX(700);
        imagenMoto2.setTranslateY(carril2PosY);
        
        velocidadCarrilSuperior = 7;
        velocidadCarrilInferior = 10;
        
        rootNivel1.getChildren().addAll(imagenCarro, imagenMoto, imagenCarro2, imagenMoto2);
        
        choque = false;
        obstaculos = new ArrayList<>();
        obstaculos.add(obstaculo);
        obstaculos.add(obstaculo2);
        obstaculos.add(obstaculo3);
        obstaculos.add(obstaculo4);
        obstaculos.add(obstaculo5);
        obstaculos.add(obstaculo6);
        obstaculos.add(obstaculo7);
        obstaculos.add(obstaculo8);
        obstaculos.add(parking);
        obstaculos.add(amarillo);
        obstaculos.add(rojo);
        obstaculos.add(amarillo);
        obstaculos.add(rojo2);
        obstaculos.add(amarillo2);
        obstaculos.add(rojo2);
        obstaculos.add(amarillo3);
        obstaculos.add(rojo3);
        obstaculos.add(amarillo4);
        obstaculos.add(rojo4);
        
        AnimationTimer animationNivel1 = new AnimationTimer()
        {
            @Override
            public void handle(long now) 
            {
                if ( (choque(imagen, imagenCarro)) == true ||
                (choque(imagen, imagenCarro2)) == true ||
                (choque(imagen, imagenMoto)) == true ||
                (choque(imagen, imagenMoto2)) == true)
                {
                    System.out.println("una vida menos");
                }
                
                imagenCarro.setTranslateX(autoPosX);
                autoPosX -= velocidadCarrilSuperior;
                if (autoPosX <- 40)
                {
                    autoPosX = 1220;
                }                
                
                imagenCarro2.setTranslateX(auto2PosX);
                auto2PosX += velocidadCarrilInferior;
                if (auto2PosX >= 1200)
                {
                    auto2PosX = 0;
                }
                
                imagenMoto.setTranslateX(motoPosX);
                motoPosX -= velocidadCarrilSuperior;
                if (motoPosX <= -40)
                {
                    motoPosX = 1220;
                }
                
                imagenMoto2.setTranslateX(moto2PosX);
                System.out.println("mot: "+imagenMoto2.getTranslateX());
                moto2PosX += velocidadCarrilInferior;
                if (moto2PosX >= 1200)
                {
                    moto2PosX = 0;
                }
                      
                pantallanivel1.setOnKeyPressed( (KeyEvent event) -> 
                {
                    moverAnimal(event);
                });
                
                choqueConObstaculo(shape, obstaculo);
                choqueConObstaculo(shape, obstaculo2);
                choqueConObstaculo(shape, obstaculo3);
                choqueConObstaculo(shape, obstaculo4);
                choqueConObstaculo(shape, obstaculo5);
                choqueConObstaculo(shape, obstaculo6);
                choqueConObstaculo(shape, obstaculo7);
                choqueConObstaculo(shape, obstaculo8);
                choqueConObstaculo(shape, parking);
                choqueConObstaculo(shape, amarillo);
                choqueConObstaculo(shape, rojo);
                choqueConObstaculo(shape, amarillo2);
                choqueConObstaculo(shape, rojo2);
                choqueConObstaculo(shape, amarillo3);
                choqueConObstaculo(shape, rojo3);
                choqueConObstaculo(shape, amarillo4);
                choqueConObstaculo(shape, rojo4);
            }
        };
           
        animationNivel1.start();
        
        stage.setScene(pantallanivel1);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }  
    
    public void moverAnimal(KeyEvent event)
    {
        for (Shape c : obstaculos)
        {
            
            if (imagen.getBoundsInParent().intersects(c.getBoundsInParent()) == true)
            {
                System.out.println(c.getBoundsInParent());
                imagen.setTranslateX(500);
                imagen.setTranslateY(500);
            }
        }
        if (imagen.getTranslateX() < 10)
        {
            imagen.setTranslateX(10);
        }
        else if (imagen.getTranslateX() > (1110))
        {
            imagen.setTranslateX(1110);
        }
        else if (imagen.getTranslateY() > 500)
        {
            imagen.setTranslateY(500);
        }
        else if (imagen.getTranslateY() < 50)
        {
            System.out.println("Gano");
        }
        else
        {
            switch(event.getCode())
            {
                case UP:
                    imagen.setTranslateY(imagen.getTranslateY() - 10);
                    shape.setTranslateY(shape.getTranslateY() - 10);
                    break;
                case RIGHT:
                    imagen.setTranslateX(imagen.getTranslateX() + 10);
                    shape.setTranslateX(shape.getTranslateX() + 10);
                    break;
                case DOWN:
                    imagen.setTranslateY(imagen.getTranslateY() + 10);
                    shape.setTranslateY(shape.getTranslateY() + 10);
                    break;
                case LEFT:
                    imagen.setTranslateX(imagen.getTranslateX() - 10);
                    shape.setTranslateX(shape.getTranslateX() - 10);
                    break;
            }  
        }
    }
    
    public boolean choque(ImageView imagen1, ImageView imagen2)
    {
        if (imagen.getBoundsInParent().intersects(imagen2.getBoundsInParent()))
                {
                    System.out.println("Choque");
                    System.out.println("AnimalitoBoundsParent :" + imagen.getBoundsInParent());
                    System.out.println("ObstaculoBoundsParent :" + imagen2.getBoundsInParent());
                    imagen.setTranslateX(500);
                    imagen.setTranslateY(500);
                    return true;
                }
        return false;
    }
    
    public void choqueConObstaculo(Shape shape, Shape obstaculo)
    {
        Shape colisionObstaculo = Shape.intersect(shape, obstaculo);
        boolean ocurrenciaColision = colisionObstaculo.getBoundsInLocal().isEmpty();
        
        if (ocurrenciaColision == false)
        {
            System.out.println("Choque con obstaculo ");
            System.out.println(shape.getTranslateX());
            System.out.println(shape.getTranslateY());
        }
    }
}
