package archivo;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Read 
{
    public static String readUsuarios()
    {
        String texto = "";
        
        try (BufferedReader reader = new BufferedReader(new FileReader("historial.txt")))
        {
            String linea = "";
            
            while ((linea=reader.readLine())!=null)
            {
                if (linea.length()!=0)
                    texto = texto.concat(linea).concat("\n");
                
            }
            
        }
        catch (FileNotFoundException e)
        {
            System.out.println("Archivo no encontrado: "+e);
        }
        catch (IOException e)
        {
            System.out.println("Problemas al manejar el archivo: "+e);
        }
        
        return texto;
    }
}

