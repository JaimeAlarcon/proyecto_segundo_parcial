/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Jugador;

import java.sql.Time;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Emilio
 * Fecha: 24-08-2018
 * Clase Jugador que crea un objteto jugador con sus atributos para ser parte de un registro y ser almacenados para luego ser serializados  
 */
@XmlType (propOrder = {"usuario","tiempo","score"})

public class Jugador {
    private String usuario;
    private Time tiempo;
    private String score;

    public Jugador(String usuario, Time tiempo, String score) {
        this.usuario = usuario;
        this.tiempo = tiempo;
        this.score = score;
    }

    public String getUsuario() {
        return usuario;
    }

    public Time getTiempo() {
        return tiempo;
    }

    public String getScore() {
        return score;
    }
   
}
