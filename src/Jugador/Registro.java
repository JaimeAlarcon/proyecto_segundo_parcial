/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Jugador;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Emilio
 * Fecha: 24-08-2018
 * Clase Registro que crea una lista de jugadores y sus datos para ser utilizada en la serialización
 */
@XmlRootElement

public class Registro {
    private List <Jugador> jugador;
    public Registro (){
        
    }

    public List<Jugador> getRegistro() {
        return jugador;
    }
    
    @XmlElement(name = "registro")
    
    public void setJugador(List<Jugador> jugador) {
        this.jugador = jugador;
    }
    
}
