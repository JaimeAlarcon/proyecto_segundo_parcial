/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Jugador;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author Emilio
 * 
 */
public class SeriaDeseria {
    private static final String HISTORIAL_XML_FILE = "historial.xml";
    private static final List<Jugador> registros=new ArrayList();
     
    
/**
 *
 * @author Emilio Dueñas
 * Fecha: 24-08-2018
 * Método para crear un archivo XML donde se guardarán los datos de los registros de los jugadores y sus datos
     * @param jugador
 * 
 */
    public static void crearXML (Jugador jugador){
        try {
            
            Registro registro = new Registro ();
            registros.add(jugador);
            
            registro.setJugador(registros);
            
            //Jaxb
            
            JAXBContext ctx = JAXBContext.newInstance(Registro.class);
            // COnversion de los objetos java a xml
            Marshaller ms = ctx.createMarshaller();
            //Preparar formato del archivo
            ms.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            // Conversion a XML
            ms.marshal(registro, new File (HISTORIAL_XML_FILE));
            
        }catch(JAXBException e) {
           System.out.println(e.getMessage());
           System.out.println("Excepción generada");
           
        }
        System.out.println("*** Se creo el archivo XML");
    }
    /**
 *
 * @author Emilio Dueñas
 * Fecha: 24-08-2018
 * Método para leer un archivo XML donde se encuentras guardados los datos de los registros de los jugadores, presenta en consola los datos
 * 
 * 
 */
      public static void leerXML (){
        try {
            //JAXB 
            JAXBContext ctx = JAXBContext.newInstance(Registro.class);
            Unmarshaller ums = ctx.createUnmarshaller();
            Registro registro = (Registro)ums.unmarshal(new File (HISTORIAL_XML_FILE));
            System.out.println("*** Se cargo el archivo XML");
            for (Jugador v: registro.getRegistro()){
                System.out.println(v.getUsuario() + " " + v.getTiempo() + " " + v.getScore());
                
            }
            
        } catch (Exception e){
            System.out.println(e.getMessage());
            System.out.println("Excepción generada...");
            
        }
}

}