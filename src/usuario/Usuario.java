package usuario;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Usuario 
{
    private static ArrayList<Usuario> listaUsuarios;
    private static String nombre;
    private static String tiempo;
    private static String monedas; 
    private static String fecha;
    private static String nivel;
    
    public Usuario(String nombre, String tiempo, String monedas, String fecha,
            String nivel)
    {
        this.nombre = nombre;
        this.tiempo = tiempo;
        this.monedas = monedas;
        this.fecha = fecha;
        this.nivel = nivel;
    }
    
    public Usuario()
    {
        this("", "", "", "", "");
    }
    
    public Usuario(String nombre)
    {
        this.nombre = nombre;
        tiempo = "";
        monedas = "";
        fecha = "";
        nivel = "";
    }
    
    public static void setListaUsuarios()
    {
        listaUsuarios = new ArrayList<>();
 
        
        for (String linea:archivo.Read.readUsuarios().split("/n"))
        {
            System.out.println(linea);
            String[] lineaSpliteada = linea.split(",");
            nombre = lineaSpliteada[0];
            tiempo = lineaSpliteada[1];
            monedas = lineaSpliteada[2];
            fecha = lineaSpliteada[3];
            nivel = lineaSpliteada[4];
            listaUsuarios.add(new Usuario(nombre, tiempo, monedas, fecha, nivel)); 
        }
        
    }
       
    public static String getNombre()
    {
        return nombre;
    }
    
    public static String getTiempo()
    {
        return tiempo;
    }
    
    public static String getMonedas()
    {
        return monedas;
    }
    
    public static String getFecha()
    {
        return fecha;
    }
    
    public static String getNivel()
    {
        return nivel;
    }
    
    public static ArrayList<Usuario> getListaUsuarios()
    {
        return listaUsuarios;
    }
    
    @Override
    public String toString()
    {
        return ("Nombre:\t"+nombre+"\n"+
                "Tiempo:\t"+tiempo+"\n"+
                "Monedas:\t"+monedas+"\n"+
                "Fecha:\t"+fecha+"\n"+
                "Nivel:\t"+nivel+"\n");
    }
}