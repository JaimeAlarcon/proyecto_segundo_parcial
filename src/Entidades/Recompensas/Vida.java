/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades.Recompensas;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Jaime Alarcón
 */
public class Vida {
     private ImageView imagenvida;
    private static final String ARCHIVO_IMAGEN="/imagenes/vida.png";
   
    /**
 *
 * Método constructor de la vida, inicializa el objeto vida
 * 
 */
    public Vida() {
        //tiempoms=3000;
        inicializarElementos();
        
    }
    public void inicializarElementos(){
        imagenvida= new ImageView(new Image(Vida.class.getResource(ARCHIVO_IMAGEN).toExternalForm()));
        imagenvida.setFitHeight(20);
        imagenvida.setFitWidth(20);
    }

    public ImageView getImagenvida() {
        return imagenvida;
    }

    public static String getARCHIVO_IMAGEN() {
        return ARCHIVO_IMAGEN;
    }
    
}
