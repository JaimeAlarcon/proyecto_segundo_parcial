/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades.Recompensas;


import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Jaime Alarcón
 */
public class Escudo {
    private ImageView imagenescudo;
    private static final String ARCHIVO_IMAGEN="/imagenes/escudo.png";
    private int tiempoms;
    private boolean interseccion;
    /**
   * 
 *
 * Método constructor del escudo, establece el tiempo de duración del escudo , además de inicializar el objeto como tal
 * 
 */
    public Escudo() {
        tiempoms=3000;
        inicializarElementos();
        interseccion = false;
        
    }
       /**
 *
 * Método para inicializar el objeto escudo, como recompensa y establecer su tamaño
 * 
 */
    public void inicializarElementos(){
        imagenescudo= new ImageView(new Image(Escudo.class.getResource(ARCHIVO_IMAGEN).toExternalForm()));
        imagenescudo.setFitHeight(20);
        imagenescudo.setFitWidth(20);
    }

    public ImageView getImagenescudo() {
        return imagenescudo;
    }

    public static String getARCHIVO_IMAGEN() {
        return ARCHIVO_IMAGEN;
    }
    
    public boolean getInterseccion()
    {
        return interseccion;
    }
    
    public void setInterseccion()
    {
        interseccion = true;
    }
}   
