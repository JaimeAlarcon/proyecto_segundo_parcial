/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades.Recompensas;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Jaime Alarcón
 */

public class Congelador {
    private ImageView imagencong;
    private static final String ARCHIVO_IMAGEN="/imagenes/congelador.png";
    private int tiempoms;
    private boolean interseccion;
    private int contadorCongelador;

 /**
 *
 * Método constructor del congelador, establece el tiempo de duración de la congelación del cronómetro, además de inicializar el objeto congelador
 * 
 */ 
    public Congelador() {
        tiempoms=3000;
        inicializarElementos();
        interseccion = false;
        contadorCongelador = 0;
    }
    public void inicializarElementos(){
        imagencong= new ImageView(new Image(Congelador.class.getResource(ARCHIVO_IMAGEN).toExternalForm()));
        imagencong.setFitHeight(20);
        imagencong.setFitWidth(20);
    }

    public ImageView getImagencongelador() {
        return imagencong;
    }

    public static String getARCHIVO_IMAGEN() {
        return ARCHIVO_IMAGEN;
    }
    
    public boolean getInterseccion()
    {
        return interseccion;
    }
    
    public void setInterseccion()
    {
        interseccion = true;
    }
    
    public int getContadorCongelador()
    {
        return contadorCongelador;
    }
    
    public void sumarContadorCongelador()
    {
        contadorCongelador += 1;
    }
}
