/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades.Recompensas;

import java.util.Random;
import java.util.stream.IntStream;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Jaime Alarcón
 */
public class Moneda {
    private ImageView imagenmoneda;
    private static final String ARCHIVO_IMAGEN="/imagenes/moneda.png";
    private IntStream valor;
    private boolean interseccion;

       /**
 *
 * Método constructor de la moneda, establece el valor de la moneda como tal, además de inicializar el objeto moneda
 * 
 */
    public Moneda() {
        Random aleatorio = new Random(System.currentTimeMillis());
// Producir nuevo int aleatorio entre 10 y 100
        valor= aleatorio.ints(10, 101);
        inicializarElementos();
        interseccion = false;
    }
    public void inicializarElementos(){
        imagenmoneda= new ImageView(new Image(Moneda.class.getResource(ARCHIVO_IMAGEN).toExternalForm()));
        imagenmoneda.setFitHeight(20);
        imagenmoneda.setFitWidth(20);
    }

    public ImageView getImagenmoneda() {
        return imagenmoneda;
    }

    public static String getARCHIVO_IMAGEN() {
        return ARCHIVO_IMAGEN;
    }

    public IntStream getValor() {
        return valor;
    }

    public void setValor(IntStream valor) {
        this.valor = valor;
    }
    
    public boolean getInterseccion()
    {
        return interseccion;
    }
    
    public void setInterseccion()
    {
        interseccion = true;
    }
}
