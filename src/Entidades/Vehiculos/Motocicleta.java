/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades.Vehiculos;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Jaime Alarcón
 */
public class Motocicleta extends Vehiculo {
        private ImageView imagenmoto;
    private static final String ARCHIVO_IMAGEN="/imagenes/moto.png";

    public Motocicleta(String nombre, String color, String tipo) {
        super(nombre, color, tipo);
        inicializarElementos();
    }
     public void inicializarElementos(){
        imagenmoto= new ImageView(new Image(Motocicleta.class.getResource(ARCHIVO_IMAGEN).toExternalForm()));
        imagenmoto.setFitHeight(80);
        imagenmoto.setFitWidth(80);
    }

    public ImageView getImagenMoto() {
        return imagenmoto;
    }

    public static String getARCHIVO_IMAGEN() {
        return ARCHIVO_IMAGEN;
    }
}
