/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades.Vehiculos;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Jaime Alarcón
 */
public class Auto extends Vehiculo {
    private ImageView imagencarro;
    private static final String ARCHIVO_IMAGEN="/imagenes/carro.png";

    public Auto(String nombre, String color, String tipo) {
        super(nombre, color, tipo);
        inicializarElementos();
    }
     public void inicializarElementos(){
        imagencarro= new ImageView(new Image(Auto.class.getResource(ARCHIVO_IMAGEN).toExternalForm()));
        imagencarro.setFitHeight(80);
        imagencarro.setFitWidth(80);
    }

    public ImageView getImagencarro() {
        return imagencarro;
    }

    public static String getARCHIVO_IMAGEN() {
        return ARCHIVO_IMAGEN;
    }
}
