/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades.Vehiculos;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Jaime Alarcón
 */
public class Camion extends Vehiculo{
    private ImageView imagencamion;
    private static final String ARCHIVO_IMAGEN="/imagenes/camion.png";

    public Camion(String nombre, String color, String tipo) {
        super(nombre, color, tipo);
        inicializarElementos();
    }
     public void inicializarElementos(){
        imagencamion= new ImageView(new Image(Camion.class.getResource(ARCHIVO_IMAGEN).toExternalForm()));
        imagencamion.setFitHeight(80);
        imagencamion.setFitWidth(80);
    }

    public ImageView getImagencamion() {
        return imagencamion;
    }

    public static String getARCHIVO_IMAGEN() {
        return ARCHIVO_IMAGEN;
    }
    
}
