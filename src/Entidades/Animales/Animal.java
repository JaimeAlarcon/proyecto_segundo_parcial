/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades.Animales;

/**
 *
 * @author Jaime Alarcón
 * Fecha: 11-08-2018
 * 
 */

public class Animal {
    private String nombre;
    private int vidas;
   private  boolean vulnerable;

   /**
 *
 * Método constructor de la clase animal
 * 
 */
    public Animal() {
        this.nombre = nombre;
        this.vidas = 3;
        this.vulnerable = true;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getVidas() {
        return vidas;
    }

    public void setVidas(int vidas) {
        this.vidas = vidas;
    }

    public boolean isVulnerable() {
        return vulnerable;
    }

    public void setVulnerable(boolean vulnerable) {
        this.vulnerable = vulnerable;
    }
    
   
   
   
}
