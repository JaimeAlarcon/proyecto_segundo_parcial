/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades.Animales;

import javafx.scene.image.Image;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 *
 * @author Jaime Alarcón
 * Fecha: 11-08-2018
 */
public class Gato extends Animal {
    private ImageView imagengato;
    private static final String ARCHIVO_IMAGEN="/imagenes/gato.png";
    
    /**
     *
     * @param nombre
     * @param vidas
     * @param vulnerable
     */
    public Gato  () {
       //super(nombre);
    
       inicializarElementos();
    
    }

/**
 *
 * Método para inicializar el objeto gato y establecer su tamaño
 * 
 */
    public void inicializarElementos(){
        imagengato= new ImageView(new Image(Gato.class.getResource(ARCHIVO_IMAGEN).toExternalForm()));
        imagengato.setFitHeight(80);
        imagengato.setFitWidth(80);
    }

    public ImageView getImagengato() {
        return imagengato;
    }

    public static String getARCHIVO_IMAGEN() {
        return ARCHIVO_IMAGEN;
    }
    

}
