/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades.Animales;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Jaime Alarcón
 * Fecha: 11-08-2018
 */
public class Vaca extends Animal{
    private ImageView imagenvaca;
    private static final String ARCHIVO_IMAGEN="/imagenes/vaca.png";
    
    /**
     *
     * @param nombre
     * @param vidas
     * @param vulnerable
     */
    public Vaca () {
       //super(nombre);
    
       inicializarElementos();
    
    }

    /**
 *
 * Método para inicializar el objeto vaca y establecer su tamaño
 * 
 */

    public void inicializarElementos(){
        imagenvaca= new ImageView(new Image(Vaca.class.getResource(ARCHIVO_IMAGEN).toExternalForm()));
        imagenvaca.setFitHeight(80);
        imagenvaca.setFitWidth(80);
    }

    public ImageView getImagenvaca() {
        return imagenvaca;
    }

    public static String getARCHIVO_IMAGEN() {
        return ARCHIVO_IMAGEN;
    }
    
}
