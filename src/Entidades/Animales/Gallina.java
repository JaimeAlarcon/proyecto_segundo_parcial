/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades.Animales;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;


/**
 *
 * @author Jaime Alarcón
 * Fecha: 11-08-2018
 * Clase gallina, extiende de la clase Animal
 */
public class Gallina extends Animal {
    private ImageView imagengallina;
    private static final String ARCHIVO_IMAGEN="/imagenes/gallina.png";
    
    /**
     *
     * @param nombre
     * @param vidas
     * @param vulnerable
     */
    public Gallina  () {
       //super(nombre);
    
       inicializarElementos();
    
    }


/**
 *
 * Método para inicializar el objeto gallina y establecer su tamaño
 * 
 */

    public void inicializarElementos(){
        imagengallina= new ImageView(new Image(Gallina.class.getResource(ARCHIVO_IMAGEN).toExternalForm()));
        imagengallina.setFitHeight(80);
        imagengallina.setFitWidth(80);
    }

    public ImageView getImagengallina() {
        return imagengallina;
    }

    public static String getARCHIVO_IMAGEN() {
        return ARCHIVO_IMAGEN;
    }
    
}
